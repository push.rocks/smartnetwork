import { tap, expect, expectAsync } from '@pushrocks/tapbundle';

import * as smartnetwork from '../ts/index.js';

let testSmartnetwork: smartnetwork.SmartNetwork;

tap.test('should create a vlid instance of SmartNetwork', async () => {
  testSmartnetwork = new smartnetwork.SmartNetwork();
  expect(testSmartnetwork).toBeInstanceOf(smartnetwork.SmartNetwork);
});

tap.test('should send a ping to Google', async () => {
  console.log(await testSmartnetwork.ping('google.com'));
  await expectAsync(testSmartnetwork.ping('google.com')).property('alive').toBeTrue();
});

tap.test('should state when a ping is not alive ', async () => {
  await expectAsync(testSmartnetwork.ping('notthere.lossless.com')).property('alive').toBeFalse();
});

tap.test('should send a ping to an IP', async () => {
  await expectAsync(testSmartnetwork.ping('192.168.186.999')).property('alive').toBeFalse();
})

tap.start();
