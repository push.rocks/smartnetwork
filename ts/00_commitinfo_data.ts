/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartnetwork',
  version: '3.0.2',
  description: 'network diagnostics'
}
