// native scope
import * as os from 'os';
import * as https from 'https';
import * as perfHooks from 'perf_hooks';

export { os, https, perfHooks };

// @pushrocks scope
import * as smartping from '@pushrocks/smartping';
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartstring from '@pushrocks/smartstring';

export { smartpromise, smartping, smartstring };

// @third party scope
// @ts-ignore
import isopen from 'isopen';
// @ts-ignore
import * as publicIp from 'public-ip';
import * as systeminformation from 'systeminformation';

export { isopen, publicIp, systeminformation };
